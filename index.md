% pages.codeberg.org/mdik
%
%

- [How to change the full disk encryption (LUKS) and user passwords in Ubuntu 20.04](./how-to-change-passwords-in-ubuntu-20.04.html)
- [How to setup sieve in Nextcloud Mail](./how-to-setup-sieve-in-nextcloud-mail.html)
