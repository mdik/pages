% How to change the full disk encryption (LUKS) and user passwords in Ubuntu 20.04
%
%

# Introduction

There are two types of "login" passwords when you installed your Ubuntu with the *full disk encryption* option.

The first one is to decrypt your hard disk. This is needed because otherwise the operating system wouldn't even have access to the files it needs to start up.

The second one is to authenticate you as a user. This password protects your computer while the screen is locked, or to authorize administrative tasks.

Both are important for their specific use cases.


# Full Disk Encryption (LUKS)

Press the *Windows* key (between *Ctrl* and *Alt*) once, and subsequently type `disks` to search for the *Disks* tool.

Click on the search result to open it.

![searching and opening the Disks utility](images/ubuntu_luks_password_change_001.png)


Then select the hard disk Ubuntu is installed on on the left-hand side. If the device identifier are more confusing than helpful, go by size, or click through them until you find a device that presents similarly to the following screenshot:

![finding and selecting the LUKS partition](images/ubuntu_luks_password_change_002.png)

When you found the hard disk, select the *LUKS partition*, identifiable by the padlock and the word "LUKS", and then click on the little button with the cog wheels to open the options menu for this partition…

![LUKS partition option menu](images/ubuntu_luks_password_change_003.png)

…where you find an entry *Change passphrase…* (passphrase is just a synonym for password).

Click it to open the dialog, enter your current password as well as the password you want to change it to (twice), and proceed by clicking *Change*.

----

If you use multiple keyboard layouts on your computer, be please aware that the keyboard layout you are using right now might be different from the one that is configured for the very first password dialog when booting your computer. If in doubt, test with characters that you know how to enter on different keyboards / keyboard layouts.

----

![LUKS partition password changing dialog](images/ubuntu_luks_password_change_004.png)

Depending on your exact setup, you might be asked to confirm this action with your login passphrase (the one that authenticates you as a user). After doing so, and rebooting, you should only be able to start your computer when you enter the new password.


# User Password

Press the *Windows* key (between *Ctrl* and *Alt*) once, and subsequently type `user` to search for the *Users* settings.

Click on the search result as shown in the following screenshot to open it.

![searching and opening the Users settings](images/ubuntu_user_password_change_001.png)

Then, click on the quite prominent *Password* field to open up the dialog to change your user password:

![the Users settings](images/ubuntu_user_password_change_002.png)

Enter your current password as well as the password you want to change it to (twice), and proceed by clicking *Change*.

![user password change dialog](images/ubuntu_user_password_change_003.png)

That's it! After you log out (or reboot) you should only be able to log in to your user account with this new password!

