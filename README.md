# pages

Built with [Pandoc](https://pandoc.org/) and a _very_ slightly modified template from [Tristano Ajmone](https://github.com/tajmone/pandoc-goodies).

[pages.codeberg.org/mdik](https://pages.codeberg.org/mdik)
